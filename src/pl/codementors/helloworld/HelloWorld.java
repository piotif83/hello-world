package pl.codementors.helloworld;

import java.util.Scanner;

/**
 * Welcome class.
 * @author psysiu
 */
public class HelloWorld {

    /**
     * Welcome method.
     * @param args Application starting parameters.
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
        
        byte byteVar;
        short shortVar;
        int intVar;
        long longVar;
        float floatVar;
        double doubleVar;
        boolean booleanVar;
        char charVar;

        Scanner inputScanner = new Scanner(System.in);
	System.out.println("Podaj liczbe typu byte");
	byteVar = inputScanner.nextByte();
	System.out.println("Podaj liczbe typu short");
	shortVar = inputScanner.nextShort();
	System.out.println("Podaj liczbe typu init");
        intVar = inputScanner.nextInt();
	System.out.println("Podaj liczbe typu long");
	longVar = inputScanner.nextLong();
	System.out.println("Podaj liczbe typu float");
	floatVar = inputScanner.nextFloat();
	System.out.println("Podaj liczbe typu double");
	doubleVar = inputScanner.nextDouble();
	System.out.println("Podaj wartość typu boolean");
	booleanVar = inputScanner.nextBoolean();

        System.out.println(byteVar);
	System.out.println(shortVar);
	System.out.println(intVar);
	System.out.println(longVar);
	System.out.println(floatVar);
	System.out.println(doubleVar);
	System.out.println(booleanVar);
    }
}

